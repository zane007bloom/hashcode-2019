import common.Common
import java.io.File

var ORIENTATION : Int = 0
var NUMBER_OF_TAGS : Int = 1

fun main() {

    //Our list of photos
    val photoList: MutableList<Photo> = buildModelFromFile()


    //DO WORK



    //TODO DONT DO THIS
    val slideList: MutableList<Slide> = mutableListOf()

    slideList.add(HorizontalSlide(0))
    slideList.add(HorizontalSlide(0))
    slideList.add(VertialSlide(1,2))
    slideList.add(VertialSlide(0, 1))

    writeOutput(slideList)
}


fun buildModelFromFile():  MutableList<Photo>{

    val lines = Common.getFile("a_example.txt").readLines()
    val totalPhotos = Integer.parseInt(lines[0])

    val photoArray: MutableList<Photo> = mutableListOf()

    for ( i in 1..totalPhotos) {

        val currentLine = lines[i]
        val currentLineSplit: List<String> = currentLine.split(" ")

        val tagList: List<String> = currentLineSplit.subList(2, 2 + Integer.parseInt(currentLineSplit[1]))

        photoArray.add( Photo(i-1, currentLineSplit[ORIENTATION], Integer.parseInt(currentLineSplit[NUMBER_OF_TAGS]), tagList))
    }

    return photoArray
}


fun writeOutput(idList: List<Slide>){

    var outputText = "" + idList.size + "\n"

    idList.forEach{slide -> outputText += slide.printSlide()}

    File("output.txt").writeText(outputText)
}

abstract class Slide {
    abstract fun printSlide() : String
}
class HorizontalSlide(val photoId: Int) : Slide() {
    override fun printSlide(): String {
        return Integer.toString(photoId) + "\n"
    }
}
data class VertialSlide(val firstPhoto: Int, val secondPhoto: Int) : Slide() {
    override fun printSlide(): String {
        return Integer.toString(firstPhoto) + " " + Integer.toString(secondPhoto) + "\n"
    }
}
data class Photo( val id: Int, val orientation: String, val numberOfTags: Int, val tags: List<String>)